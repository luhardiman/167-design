<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'product';

    protected $fillable = [
        'name', 'description', 'price', 'img'
    ];

    public function sizes() {
        return $this->hasManyThrough(size::class, product_size::class, "productFk", "id", "id", "sizeFk");
    }
}
