<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_size', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('productFk');
            $table->unsignedInteger('sizeFk');

            $table->foreign('productFk')->references('id')->on('product')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('sizeFk')->references('id')->on('size')->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->unique(array('productFk', 'sizeFk'));


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_size');
    }
}
