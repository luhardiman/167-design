<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = array(
            [
                "name" => "Armband 1",
                "description" => "This is the first!",
                "price" => 50,
                "img" => "path/to/img"
            ],
            [
                "name" => "Armband 2",
                "description" => "This is the first!",
                "price" => 50,
                "img" => "path/to/img"
            ]
        );

        foreach ($products as $product) {
            \App\product::create($product);
        }
    }
}
