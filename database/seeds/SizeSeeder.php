<?php

use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = array(
            [
                "size" => "180cm"
            ],
            [
                "size" => "160cm"
            ],
            [
                "size" => "100cm"
            ]
        );

        foreach ($sizes as $size) {
            \App\size::create($size);
        }
    }
}
